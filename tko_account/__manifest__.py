# © 2019 TKOpen <https://tkopen.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    'name': 'Account Fixings',
    'summary': 'Account Fixings',
    'description': 'Account Fixings',
    'author': 'TKOpen',
    'category': 'Accounting',
    'license': 'LGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.0.2',
    'support': 'info@tkopen.com',
    'sequence': 1,
    'depends': [
        'account',
    ],
    'data': [
        'views/res_partner_view.xml',
    ],
    'init_xml': [],
    'update_xml': [],
    'css': [],
    'demo_xml': [],
    'test': [],
    'external_dependencies': {
        'python': [
            'numpy',
        ],
        'bin': [],
    },
    'images': ['static/description/banner.png'],
    'application': True,
    'installable': True,
    'auto_install': False,
}
