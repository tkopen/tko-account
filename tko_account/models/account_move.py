import json
import datetime
from odoo import models
ALLOWED_BUSDAYS = 5
import logging

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = 'account.move'

    # def js_remove_outstanding_partial(self, partial_id):
    #     ''' Called by the 'payment' widget to remove a reconciled entry to the present invoice.
    #
    #     :param partial_id: The id of an existing partial reconciled with the current invoice.
    #     '''
    #     self.ensure_one()
    #     super(AccountMove, self).js_remove_outstanding_partial(partial_id)
    #     self._compute_payment_state()
    #     return True
