import json
import datetime
from odoo import models, fields
import logging

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    ledger_balance = fields.Float('Balance', compute='_get_ledger_balance')

    def _get_ledger_balance(self):
        for partner in self:
            # Get receivable balance (for customers)
            receivable_lines = self.env['account.move.line'].search([
                ('partner_id', '=', partner.id),
                ('account_id.account_type', '=', 'asset_receivable'),
                ('move_id.state','=','posted')
            ])
            receivable_balance = sum(receivable_lines.mapped('balance'))

            # Get payable balance (for suppliers)
            payable_lines = self.env['account.move.line'].search([
                ('partner_id', '=', partner.id),
                ('account_id.account_type', '=', 'liability_payable'),
                ('move_id.state','=','posted')
            ])
            payable_balance = sum(payable_lines.mapped('balance'))

            # For customers (receivables are positive, payables are negative)
            partner.ledger_balance = receivable_balance - payable_balance
    def open_partner_ledger(self):
        """Redirect to account.move.line tree view filtered by the partner"""
        return {
            'type': 'ir.actions.act_window',
            'name': 'Partner Ledger',
            'res_model': 'account.move.line',
            'view_mode': 'tree,form',
            'domain': [('partner_id', '=', self.id), ('account_id.account_type', 'in',[ 'asset_receivable','liability_payable'])],  # Filters by the selected partner
            'context': {'search_default_partner_id': self.id},  # Default search filter for the partner
            'target': 'current',
        }