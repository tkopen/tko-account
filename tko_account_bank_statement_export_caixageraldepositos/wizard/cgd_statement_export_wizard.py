from odoo import models, fields, api
from datetime import datetime
import tempfile
import base64
from odoo.addons.tko_account_invoice_supplier_payment_methods.models.account_invoice import PAYMENT_TYPES

YEARS_VALS = [(i, i) for i in range(2016, datetime.utcnow().year + 1)]
import logging
from odoo.exceptions import UserError
_logger = logging.getLogger(__name__)


class CgdStatementExportWizard(models.TransientModel):
    _name = 'cgd.statement.export.wizard'
    _description = 'CGD Statement Export Wizard'

    invoice_ids = fields.Many2many('account.move', string='Invoices')
    m_url = fields.Char('Multibanco', readonly=True)
    bt_url = fields.Char('Bank Transference', readonly=True)
    pe_url = fields.Char('Pagamento ao Estado', readonly=True)

    # year = fields.Selection(YEARS_VALS, 'Year')

    @api.model
    def default_get(self, fields):
        context = self.env.context
        result = super(CgdStatementExportWizard, self).default_get(fields)
        domain = [('move_type', '=', 'in_invoice'), ('state', '=', 'posted'), ('approval_status', '=', 'a'),
                  ('date_payment', '=', datetime.today().date())]
        invoices = self.env['account.move'].search(domain)
        result['invoice_ids'] = [(6, 0, invoices.ids)]
        return result

    def export_reports(self):
        def invoice_bank_transferance_text(invoice, batch_reference):
            if invoice:
                if not invoice.partner_bank_id.acc_number:
                    raise UserError("Bank account not set for partner in Invoice : %s" %invoice.name)
                col_f = 'PRIMESCHOOL %s' % (invoice.vendor_reference or '')
                col_A = invoice.partner_bank_id.acc_number.replace(" ", "")
                col_B = invoice.partner_id.name[:27]
                col_C = invoice.partner_bank_id.bank_id.bic or ''
                col_D = invoice.amount_residual
                col_E = batch_reference
                col_F = col_f[:140]
                return "%s\t%s\t%s\t%s\t%s\t%s\n" % (col_A, col_B, col_C, col_D, col_E, col_F)

        def invoice_multibanco_text(invoice):
            if invoice:
                col_A = invoice.multibanco_entidade
                col_B = invoice.payment_reference
                col_C = invoice.amount_residual
                return "%s\t%s\t%s\n" % (col_A, col_B, col_C)

        def invoice_pagamento_estasdo_text(invoice):
            if invoice:
                col_A = invoice.payment_reference
                col_B = invoice.amount_residual
                return "%s\t%s\n" % (col_A, col_B)

        payment_types = set(self.invoice_ids.mapped('vendor_payment_type'))
        for payment_type in payment_types:
            payment_type_str = dict(PAYMENT_TYPES).get(payment_type)
            datetime_str = datetime.strftime(datetime.now(), '%Y%m%d%H%M')

            _logger.info("GENERATING %s .TXT  file for : Caixa Geral de Depósitos ... " % payment_type_str)
            tmp = tempfile.NamedTemporaryFile()
            with open(tmp.name, 'a+') as f:
                for invoice in self.invoice_ids.filtered(lambda l: l.vendor_payment_type == payment_type):
                    batch_reference = 'T' + datetime_str + str(invoice.id).zfill(8)  # 21 chars
                    if payment_type == 'bt':
                        f.write(invoice_bank_transferance_text(invoice, batch_reference))
                    if payment_type == 'm':
                        f.write(invoice_multibanco_text(invoice))
                    if payment_type == 'pe':
                        f.write(invoice_pagamento_estasdo_text(invoice))
                    ### Update Bank Batch Reference
                    invoice.write({'bank_batch_reference': datetime_str})
                f.seek(0)
                filename = datetime_str + ' ' + payment_type_str + '.txt'
                values = {
                    'name': filename,
                    'res_model': 'cgd.statement.export.wizard',
                    'res_id': False,
                    'type': 'binary',
                    'public': True,
                    'datas': base64.b64encode(f.read().encode('utf-8'))
                }
                attachment_id = self.env['ir.attachment'].sudo().create(values)
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
                download_url = str(base_url) + str(download_url)
                if payment_type == 'm':
                    self.m_url = download_url
                if payment_type == 'bt':
                    self.bt_url = download_url
                if payment_type == 'pe':
                    self.pe_url = download_url

        view_id = self.env.ref('tko_account_bank_statement_export_caixageraldepositos.cgd_statement_export_wizard')
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'cgd.statement.export.wizard',
            'res_id': self.id,
            'view_id': view_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
