# © 2019 TKOpen <https://tkopen.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'tko_account_bank_statement_export_caixageraldepositos',
    'summary': 'tko_account_bank_statement_export_caixageraldepositos',
    'description': '',
    'author': 'TKOpen',
    'category': 'Account',
    'license': 'AGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.1',
    'sequence': 1,
    'depends': [
        'tko_account_invoice_supplier_payment_methods',
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/cgd_statement_export_view.xml',
    ],
    'external_dependencies': {
        'python': [],
    },
    'installable': True,
    'application': False,
    'auto_install': False,
}
